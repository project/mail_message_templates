<?php

namespace Drupal\mail_message_templates\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Mail message entity.
 *
 * @ConfigEntityType(
 *   id = "mail_message_template",
 *   label = @Translation("Mail Message Template"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mail_message_templates\MailMessageTemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\mail_message_templates\Form\MailMessageTemplateForm",
 *       "edit" = "Drupal\mail_message_templates\Form\MailMessageTemplateForm",
 *       "delete" = "Drupal\mail_message_templates\Form\MailMessageTemplateDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\mail_message_templates\MailMessageTemplateHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "template",
 *   config_export = {
 *     "id",
 *     "subject",
 *     "body_lines",
 *     "notes",
 *     "status"
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "id",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/content/mail_message_templates/{mail_message_template}",
 *     "add-form" = "/admin/config/content/mail_message_templates/add",
 *     "edit-form" = "/admin/config/content/mail_message_templates/{mail_message_template}/edit",
 *     "delete-form" = "/admin/config/content/mail_message_templates/{mail_message_template}/delete",
 *     "collection" = "/admin/config/content/mail_message_templates"
 *   }
 * )
 */
class MailMessageTemplate extends ConfigEntityBase implements MailMessageTemplateInterface {

  /**
   * The Mail message key.
   *
   * @var string
   */
  protected $id;

  /**
   * The Mail message subject.
   *
   * @var string
   */
  protected $subject;

  /**
   * The Mail message body.
   *
   * @var string
   */
  protected $body_lines = '';

  /**
   * The Mail message developer notes.
   *
   * @var string
   */
  protected $notes;

  /**
   * The enabled status of this template.
   *
   * @var bool
   */
  protected $status;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // Chaining \r\n to \n so we get multiple lines in the yml
    // See https://www.drupal.org/node/3114725
    $this->body_lines = str_replace("\r\n", "\n", $this->body_lines);
  }

}

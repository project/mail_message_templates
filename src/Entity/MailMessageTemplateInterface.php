<?php

namespace Drupal\mail_message_templates\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Config entity for storing mail message templates.
 */
interface MailMessageTemplateInterface extends ConfigEntityInterface {


}

<?php

namespace Drupal\mail_message_templates;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Mail message entities.
 */
class MailMessageTemplateListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_ids = $this->getEntityIds();
    $entities = $this->storage->loadMultipleOverrideFree($entity_ids);

    // Sort by ID.
    uasort($entities, function (ConfigEntityInterface $a, ConfigEntityInterface $b) {
      return $a->id() > $b->id() ? -1 : 1;
    });

    // Sort by status.
    uasort($entities, function (ConfigEntityInterface $a, ConfigEntityInterface $b) {
      return $a->status() > $b->status() ? -1 : 1;
    });

    return $entities;
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle() {
    return $this->t("Mail Message Templates");
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Message ID');
    $header['status'] = $this->t('Status');
    $header['subject'] = $this->t('Mail Subject');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof ConfigEntityInterface);

    $attributes = [];
    if (!$entity->status()) {
      $attributes['style'] = 'opacity: 0.5';
    }

    $row['id'] = ['data' => $entity->id()] + $attributes;
    $row['status'] = [
      'data' => $entity->status() ? $this->t('Enabled') : $this->t('Disabled'),
    ] + $attributes;
    $row['subject'] = [
      'data' => $entity->get('subject'),
    ] + $attributes;

    return [
      'class' => [$entity->status() ? 'enabled' : 'disabled'],
      'data' => $row + parent::buildRow($entity),
    ];
  }

}

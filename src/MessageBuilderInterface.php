<?php

namespace Drupal\mail_message_templates;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Service class for building email messages from config defined by this module.
 */
interface MessageBuilderInterface {

  /**
   * MailMessageBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager for loading config.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   State for saving paramater hints.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $languageManager, StateInterface $state);

  /**
   * Builds out the mail message based on the mail id and params.
   *
   * @param string $id
   *   The full ID of the mail e.g. module_name-mail-key.
   * @param array $message
   *   The mail message built so far by Drupal.
   * @param array $params
   *   Params used for text replacements.
   */
  public function build($id, array &$message, array $params);

  /**
   * Converts params array to string placeholders.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $subject
   *   The message subject.
   * @param array $body_lines
   *   Array of the message.
   * @param array $params
   *   Array of parameters.
   * @param string $langcode
   *   The language code.
   *
   * @return array
   *   An array of keyed with placeholder names.
   */
  public function buildPlaceholders($subject, array $body_lines, array $params, $langcode);

}

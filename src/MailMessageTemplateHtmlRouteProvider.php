<?php

namespace Drupal\mail_message_templates;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Mail message entities.
 *
 * @see \Drupal\entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\entity\Routing\DefaultHtmlRouteProvider
 */
class MailMessageTemplateHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    // Provide your custom entity routes here.
    return $collection;
  }

}

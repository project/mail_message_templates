<?php

namespace Drupal\mail_message_templates\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mail_message_templates\Entity\MailMessageTemplate;

/**
 * Class MailMessageForm.
 */
class MailMessageTemplateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $template = $this->entity;
    assert($template instanceof MailMessageTemplate);

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message ID'),
      '#description' => 'e.g. my_module-my-message',
      '#default_value' => $template->id(),
      '#required' => TRUE,
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#default_value' => $template->get('subject'),
      '#description' => $this->t('Available Parameters may be used as token replacements'),
    ];

    $form['body_lines'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $template->get('body_lines'),
      '#description' => $this->t('Available Parameters may be used as token replacements'),
    ];

    $params = \Drupal::state()->get('mail_message_config_entity.' . $template->id() . '.params', []);
    $params = array_filter($params, function (string $k, $v) {
      if (str_contains($k, 'url')) {
        return $k[0] == ':';
      }
      return $k[0] == '@';
    }, ARRAY_FILTER_USE_BOTH);

    $form['params'] = [
      '#title' => 'Detected parameters',
      '#type' => 'fieldset',
      'params' => ['#markup' => '<code>' . implode(" ", $params) . '</code>'],
      '#description' => $this->t("These will be detected and populated once a message with this id is sent. <br>As a general rule use '@' prefix for text and ':' prefix for URLs. <br>see <a href='https://api.drupal.org/api/drupal/core!lib!Drupal!Component!Render!FormattableMarkup.php/function/FormattableMarkup%3A%3AplaceholderFormat'>FormattableMarkup::placeholderFormat</a>"),
    ];

    $form['notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Developer Notes'),
      '#default_value' => $template->get('notes'),
      '#description' => $this->t('Use this to store additional information about this email such as when it is sent and why, parameter details etc'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $template->isNew() ? TRUE : $template->get('status'),
      '#description' => $this->t('Check this to use this template to replace message text'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $template = $this->entity;
    $status = $template->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Mail message.', [
          '%label' => $template->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Mail message.', [
          '%label' => $template->label(),
        ]));
    }
    $form_state->setRedirectUrl($template->toUrl('collection'));
  }

}

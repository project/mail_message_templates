<?php

namespace Drupal\mail_message_templates\EventSubscriber;

use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 */
class LanguageConfigOverrideEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {

    if (!class_exists(LanguageConfigOverrideEvents::class)) {
      return [];
    }

    return [
      LanguageConfigOverrideEvents::SAVE_OVERRIDE => 'saveOverride',
    ];
  }

  /**
   * React to a language config override object being saved.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   Config crud event.
   */
  public function saveOverride(LanguageConfigOverrideCrudEvent $event) {
    $config = $event->getLanguageConfigOverride();

    // Chaning \r\n to \n so we get multiple lines in the yml
    // See https://www.drupal.org/node/3114725
    if (preg_match('/^mail_message_templates\.template\./', $config->getName())) {
      $lines = $config->get('body_lines');
      $config->set('body_lines', str_replace("\r\n", "\n", $lines));
      $config->getStorage()->write($config->getName(), $config->get());
    }

  }

}

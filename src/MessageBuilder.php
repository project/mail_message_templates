<?php

namespace Drupal\mail_message_templates;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mail_message_templates\Entity\MailMessageTemplateInterface;

/**
 * Service class for building email messages from config defined by this module.
 */
class MessageBuilder implements MessageBuilderInterface {


  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * State API.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritDoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, StateInterface $state) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->state = $state;
  }

  /**
   * {@inheritDoc}
   */
  public function build($id, array &$message, array $params) {

    $template_storage = $this->entityTypeManager->getStorage('mail_message_template');
    $original_language = $this->languageManager->getConfigOverrideLanguage();
    $message_language = $message['langcode'] ? $this->languageManager->getLanguage($message['langcode']) : $this->languageManager->getDefaultLanguage();

    // Load the config based on the message langcode.
    $this->languageManager->setConfigOverrideLanguage($message_language);
    $template = $template_storage->load($id);
    $this->languageManager->setConfigOverrideLanguage($original_language);

    $original_subject = $params['context']['subject'] ?? $message['subject'];
    $body_lines = $params['context']['message'] ?? $message['body'];
    $original_body_lines = is_array($body_lines) ? $body_lines : [$body_lines];

    $placeholders = $this->buildPlaceholders($original_subject, $original_body_lines, $params, $message_language->getId());
    $tokens = array_keys($placeholders);
    $values = array_values($placeholders);

    // Formattable markup is used to nicely use the token replacement from core.
    // This creates a #markup object that symfony mailer will convert to plain
    // text objects, stripping the new lines.
    // This causes all our emails to be sent in one long string.
    // Casting the formattableMarkup to a string avoids this.
    // If we want to create mail message templates in the future with markup,
    // we will need to add a checkbox to use HTML in the template config and
    // wrap the casting in an if statement.
    if ($template instanceof MailMessageTemplateInterface && $template->status()) {
      $message['subject'] = (string) (new FormattableMarkup($template->get('subject'), $placeholders));
      $message['body'] = [(string) (new FormattableMarkup($template->get('body_lines'), $placeholders))];
    }
    elseif (!$template) {
      $subject = $original_subject;
      if ($subject instanceof TranslatableMarkup) {
        $subject = $subject->getUntranslatedString();
      }

      $lines = [];
      foreach ($original_body_lines as $line) {
        if ($line instanceof TranslatableMarkup) {
          $lines[] = $line->getUntranslatedString();
        }
        else {
          $lines[] = $line;
        }
      }
      // Convert to the editable double new lines form of the lines.
      $lines = implode(PHP_EOL . PHP_EOL, $lines);

      $template = $template_storage->create([
        'id' => $id,
        'body_lines' => $lines,
        'subject' => $subject,
        'status' => FALSE,
      ]);
      $template->save();
    }

    // Register mail params to be used in form help text.
    $params_saved = &drupal_static($id . ':saved_params', FALSE);
    if (!$params_saved) {
      $placeholders = array_keys($placeholders);
      $this->state->set('mail_message_config_entity.' . $id . '.params', $placeholders);
      $params_saved = TRUE;

      /** @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $kvStorage */
      $kvStorage = \Drupal::service('keyvalue.expirable')->get('mail_message_template_param_hints');
      $kvStorage->setWithExpire($id, $placeholders, 86400 * 30);
    }

  }

  /**
   * {@inheritDoc}
   */
  public function buildPlaceholders($subject, array $body_lines, array $params, $langcode) {

    $placeholders = [];

    if ($subject instanceof TranslatableMarkup) {
      foreach ($subject->getArguments() as $arg => $value) {
        $arg = substr($arg, '1');
        $placeholders["@$arg"] = $value;
        $placeholders["%$arg"] = $value;
        $placeholders[":$arg"] = $value;
      }
    }

    foreach ($body_lines as $line) {
      if ($line instanceof TranslatableMarkup) {
        foreach ($line->getArguments() as $arg => $value) {
          $arg = substr($arg, '1');
          $placeholders["@$arg"] = $value;
          $placeholders["%$arg"] = $value;
          $placeholders[":$arg"] = $value;
        }
      }
    }

    unset($params['context']);

    foreach ($params as $param_name => $value) {
      try {

        if ($value instanceof EntityInterface) {

          if ($value instanceof TranslatableInterface) {
            $value = $value->getTranslation($langcode);
          }

          $value = (string) $value->label();
        }
        elseif (is_object($value) && method_exists($value, '__toString')) {
          $value = (string) $value;
        }

        if (!is_scalar($value)) {
          continue;
        }

        $placeholders["@$param_name"] = $value;
        $placeholders["%$param_name"] = $value;
        $placeholders[":$param_name"] = $value;
      }
      catch (\Exception $ex) {

      }
    }

    return $placeholders;
  }

}

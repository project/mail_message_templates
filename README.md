# Mail Message Templates
Provides email message templates as configuration entities.

## Example code to send emails.
```php
/** @var \Drupal\Core\Mail\MailManagerInterface $mail_manager */
$mail_manager = \Drupal::service('plugin.manager.mail');
$params = ['my_param' => "My custom param text"];
$mail_manager->mail('my_module', 'my-email-message', 'test@example.com', 'en', $params);
$mail_manager->mail('my_module', 'my-email-message', 'test@example.com', 'fr', $params);
```
